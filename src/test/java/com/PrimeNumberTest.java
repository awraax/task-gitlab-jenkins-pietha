package com;

import org.junit.jupiter.api.*;

class PrimeNumberTest {

    public PrimeNumber prime;

    @BeforeEach
    public void setUp() {
        prime = new PrimeNumber();
    }

    @DisplayName("Number 7 is a prime expected return is Prime Number")
    @Test
    public void testPrime() {
        String result = prime.play(7);
        Assertions.assertEquals(result, "Prime Number");
    }

    @DisplayName("Number 5 is a prime expected return is Prime Number")
    @Test
    public void testPrimeTwo() {
        String result = prime.play(5);
        Assertions.assertEquals(result, "Prime Number");
    }

    @DisplayName("Number 3 is a prime expected return is Prime Number")
    @Test
    public void testPrimeThree() {
        String result = prime.play(3);
        Assertions.assertEquals(result, "Prime Number");
    }

    @DisplayName("Number 9 is a multiple 3 expected return is Astra")
    @Test
    public void testPrimeFour() {
        String result = prime.play(9);
        Assertions.assertEquals(result, "Astra");
    }

    @DisplayName("Number 30 is a multiple 3 and 5 expected return is AstraPay")
    @Test
    public void testPrimeFive() {
        String result = prime.play(30);
        Assertions.assertEquals(result, "AstraPay");
    }

    @DisplayName("Number 20 is a multiple 5 expected return is Pay")
    @Test
    public void testPrimeSix() {
        String result = prime.play(20);
        Assertions.assertEquals(result, "Pay");
    }

    @DisplayName("Number 16 not prime or multiple 3 and 5")
    @Test
    public void testPrimeSeven() {
        String result = prime.play(16);
        Assertions.assertEquals(result, "Not prime or multipe 3 & 5");
    }
    
    // failed test case
    //@DisplayName("Number 16 not prime or multiple 3 and 5")
    //@Test
    //public void testPrimeEight() {
      //  String result = prime.play(16);
        //Assertions.assertEquals(result, "Astrapay");
    //}



    @AfterEach
    public void tearDown() {
        prime = null;
    }

}
