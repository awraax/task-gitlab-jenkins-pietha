package com;

public class PrimeNumber {

    //this method to check if number is a prime number or not
    public boolean isPrime(int num) {
        if (num <= 1) {  
            return false;  
        }  
        for (int i = 2; i <= Math.sqrt(num); i++) {  
            if (num % i == 0) {  
                return false;  
            }  
        }  
        return true;  
    }  

    public String play(int num) {
        boolean isNumPrime = this.isPrime(num);
        if (isNumPrime) {
            return "Prime Number";
        } else {
            if (num % 3 == 0 && num % 5 == 0) {
                return "AstraPay";
            } else if (num % 3 == 0){
                return "Astra";
            } else if (num % 5 == 0) {
                return "Pay";
            }
        }

        return "Not prime or multipe 3 & 5";
    }
    
}